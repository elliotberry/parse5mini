const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = {
  target: "webworker",
  entry: "./lib/index.js",
  mode: "production",
  plugins: [
    new BundleAnalyzerPlugin()
  ]
}
